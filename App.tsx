import React from 'react';
import {TamaguiProvider} from 'tamagui';

import config from './tamagui.config';
import {Text, View} from 'react-native';

const App = () => {
  return (
    <TamaguiProvider config={config}>
      <View>
        <Text>hello world</Text>
      </View>
    </TamaguiProvider>
  );
};

export default App;
